<?php

namespace Drupal\content_modification_log\Controller;

use Drupal;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileSystem;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class ContentModificationLogExportController
 *
 * @package Drupal\content_modification_log\Controller
 */
class ContentModificationLogExportController extends ControllerBase {

  /**
   * @return array
   */
  public function content(): array
  {

    $config = $this->config('content_modification_log.settings');

    $start_date = Drupal::request()->query->get('start');
    $end_date = Drupal::request()->query->get('end');

    if ($start_date) {
      $start_date = strtotime($start_date);
    }
    if ($end_date) {
      $end_date = strtotime($end_date . ' +1 day');
    }

    $build['filter_form'] = \Drupal::formBuilder()
      ->getForm('Drupal\content_modification_log\Form\ContentModificationLogFilterForm');

    $header = [
      ['data' => t('Entity ID'), 'field' => 'cml.entity_id'],
      ['data' => t('Title'), 'field' => 'cml.entity_title'],
      ['data' => t('Revision Log Message'), 'field' => 'cml.revision_log_message'],
      ['data' => t('Entity Type'), 'field' => 'cml.entity_type'],
      ['data' => t('Action'), 'field' => 'cml.action'],
      ['data' => t('Author'), 'field' => 'ufd.name'],
      ['data' => t('IP Address'), 'field' => 'cml.client_ip'],
      ['data' => t('Updated'), 'field' => 'cml.timestamp', 'sort' => 'desc'],
    ];

    $query = Drupal::database()->select('content_modification_log', 'cml')
      ->extend('Drupal\Core\Database\Query\TableSortExtender');

    if (!empty($start_date) && empty($end_date)) { // Just a start date.
      $query->condition('timestamp', $start_date, '>=');
    } elseif (empty($start_date) && !empty($end_date)) { // Just an end date.
      $query->condition('timestamp', $end_date, '<=');
    } elseif (!empty($start_date) && !empty($end_date)) { // Both start and end dates.
      $query->condition('timestamp', [$start_date, $end_date], 'BETWEEN');
    }

    $query->join('users_field_data', 'ufd', 'cml.uid = ufd.uid');
    $query->fields('cml', [
      'lid',
      'uid',
      'timestamp',
      'client_ip',
      'entity_type',
      'entity_id',
      'entity_title',
      'entity_bundle',
      'revision_log_message',
      'action'
    ]);
    $query->fields('ufd', [
      'name'
    ]);
    $query->orderByHeader($header);

    $page_rowcount = (($config->get('cml_rowcount')) ?: 50);
    $pager = $query
      ->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->limit($page_rowcount);

    $results = $pager->execute()->fetchAll();

    $nids = [];
    $fids = [];
    foreach ($results as $result) {
      if ($result->entity_type == 'node' && !in_array($result->entity_id, $nids)) {
        $nids[] = $result->entity_id;
      }
      if ($result->entity_type == 'file' && !in_array($result->entity_id, $fids)) {
        $fids[] = $result->entity_id;
      }
    }

    $nodes = Node::loadMultiple($nids);
    $files = File::loadMultiple($fids);

    $rows = [];
    foreach ($results as $result) {

      $entity_link = NULL;

      if ($result->entity_type == 'node') {
        if (array_key_exists($result->entity_id, $nodes)) {
          $entity_link = Link::fromTextAndUrl(
            $result->entity_title,
            Url::fromUserInput('/node/' . $result->entity_id)
          );
        }
      }elseif ($result->entity_type == 'file') {
        if (array_key_exists($result->entity_id, $files)) {
          $entity_link = Link::fromTextAndUrl(
            $result->entity_title,
            Url::fromUserInput($files[$result->entity_id]->createFileUrl())
          );
        }
      }


      $user_link = Link::fromTextAndUrl(
        $result->name,
        Url::fromUserInput('/user/' . $result->uid)
      );

      if ($result->uid == 0) {
        $user_link = 'System Updates';
      }

      $rows[] = ['data' => [
        'cml.entity_id' => $result->entity_id,
        'cml.title' => (($entity_link) ?: $result->entity_title),
        'cml.revision_log_message' => $result->revision_log_message,
        'cml.entity_type' => $result->entity_type,
        'cml.action' => $result->action,
        'ufd.name' => $user_link,
        'cml.client_ip' => $result->client_ip,
        'cml.timestamp' => Drupal::service('date.formatter')
          ->format($result->timestamp, 'short'),
      ]];

    }

    $build['table'] = [
      '#type' => 'table', // '#type' => 'tableselect',
      '#header' => $header,
      '#rows' => $rows,   // '#options' => $rows,
      '#empty' => t('No log entries found.'),
    ];

    $build['pager'] = array(
      '#type' => 'pager'
    );

    return $build;
  }

  /**
   * @return RedirectResponse
   */
  public function export_csv(): RedirectResponse
  {

    $config = $this->config('content_modification_log.settings');

    $start_date = Drupal::request()->query->get('start');
    $end_date = Drupal::request()->query->get('end');

    if ($start_date) {
      $start_date = strtotime($start_date);
    }
    if ($end_date) {
      $end_date = strtotime($end_date . ' +1 day');
    }

    $query = Drupal::database()->select('content_modification_log', 'cml');

    $query->join('users_field_data', 'ufd', 'cml.uid = ufd.uid');
    $query->fields('cml', [
      'lid',
      'uid',
      'timestamp',
      'client_ip',
      'entity_type',
      'entity_title',
      'revision_log_message',
      'entity_id',
      'entity_bundle',
      'action'
    ]);
    $query->fields('ufd', [
      'name'
    ]);

    if (!empty($start_date) && empty($end_date)) { // Just a start date.
      $query->condition('timestamp', $start_date, '>=');
    } elseif (empty($start_date) && !empty($end_date)) { // Just an end date.
      $query->condition('timestamp', $end_date, '<=');
    } elseif (!empty($start_date) && !empty($end_date)) { // Both start and end dates.
      $query->condition('timestamp', [$start_date, $end_date], 'BETWEEN');
    }

    $results = $query->execute()->fetchAll();

    $header = [
      'lid',
      'uid',
      'username',
      'timestamp',
      'client_ip',
      'entity_id',
      'entity_title',
      'revision_log_message',
      'entity_type',
      'entity_bundle',
      'entity_url',
      'action',
    ];

    $nids = [];
    $fids = [];
    foreach ($results as $result) {
      if ($result->entity_type == 'node' && !in_array($result->entity_id, $nids)) {
        $nids[] = $result->entity_id;
      }
      if ($result->entity_type == 'file' && !in_array($result->entity_id, $fids)) {
        $fids[] = $result->entity_id;
      }
    }

    $nodes = Node::loadMultiple($nids);
    $files = File::loadMultiple($fids);

    $rows[] = $header; // Set first row to header.

    foreach ($results as $result) {

      $entity_url = NULL;
      if ($result->entity_type == 'node') {
        if (array_key_exists($result->entity_id, $nodes)) {
          $entity_url = Url::fromUserInput('/node/' . $result->entity_id, [
            'absolute' => TRUE
          ])->toString();
        }
      }elseif ($result->entity_type == 'file') {
        if (array_key_exists($result->entity_id, $files)) {
          $entity_url = Url::fromUserInput($files[$result->entity_id]->createFileUrl(), [
            'absolute' => TRUE
          ])->toString();
        }
      }

      $rows[] = [
        'lid' => $result->lid,
        'uid' => $result->uid,
        'username' => $result->name,
        'timestamp' => Drupal::service('date.formatter')->format($result->timestamp, 'short'),
        'client_ip' => $result->client_ip,
        'entity_id' => $result->entity_id,
        'entity_title' => $result->entity_title,
        'revision_log_message' => $result->revision_log_message,
        'entity_type' => $result->entity_type,
        'entity_bundle' => $result->entity_bundle,
        'entity_url' => $entity_url,
        'action' => $result->action,
      ];
    }

    // Replace tokens in CSV Filename (if they have been set).
    $csv_filename = Drupal::service('token')->replace(($config->get('cml_csv_filename')) ?: 'content-log.csv');

    // Store csv string in variable.
    $handle = fopen('php://memory', 'w');
    foreach( $rows as $row) { //} $offset => $row) {
      fputcsv($handle, (array) $row);
    }
    fseek($handle, 0);

    // Get the CSV string.
    $csv = stream_get_contents($handle);
    $csv = mb_convert_encoding($csv, 'iso-8859-2', 'utf-8');

    // Prepare the directory (in case it doesn't exist), and save the file.
    $directory = 'public://content_logs/';
    Drupal::service('file_system')->prepareDirectory($directory, FileSystem::CREATE_DIRECTORY);
    $fileRepository = Drupal::service('file.repository');
    $fileRepository->writeData($csv, $directory . $csv_filename, FileSystemInterface::EXISTS_REPLACE);

    $csv_url = Drupal::service('file_url_generator')->generateAbsoluteString($directory . $csv_filename);
    Drupal::messenger()->addStatus(t('The CSV was created: <a href=":csv_url?cb=:cache_buster">@csv_filename</a>.', [
      ':csv_url' => $csv_url,
      ':cache_buster' => time(),
      '@csv_filename' => $csv_filename,
    ]));

    $params = [];
    if (!empty($start_date)) {
      $params['start'] = date('Y-m-d', $start_date);
    }
    if (!empty($end_date)) {
      $end_date = strtotime(date('Y-m-d', $end_date) . ' -1 day');
      $params['end'] = date( 'Y-m-d', $end_date);
    }

    return $this->redirect('content_modification_log.report', $params);
  }

}
