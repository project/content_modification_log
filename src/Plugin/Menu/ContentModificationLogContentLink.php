<?php

namespace Drupal\content_modification_log\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;

class ContentModificationLogContentLink extends MenuLinkDefault {

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    $config = \Drupal::config('content_modification_log.settings');
    if ($config->get('show_tab') !== 1) {
      return FALSE;
    }
    return TRUE;
  }
}
