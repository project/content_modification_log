<?php

/**
 * @file
 * Contains \Drupal\content_modification_log\Form\ContentModificationLogFilterForm
 */

namespace Drupal\content_modification_log\Form;

use Drupal;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;


class ContentModificationLogFilterForm extends FormBase
{

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return 'content_modification_log_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array
  {

//    $config = $this->config('content_modification_log.settings');

    $form['export_to_csv'] = [
      '#type' => 'submit',
      '#value' => $this->t('+ Export to CSV'),
      '#name' => 'export_results',
      '#button_type' => 'primary',
      '#attributes' => [
        'class' => [
          'button--small'
        ],
      ],
    ];


    $form['content_log_filters'] = [
      '#type' => 'container',
    ];

    $form['content_log_filters']['date_range'] = [
      '#type' => 'details',
      '#title' => $this->t('Dates'),
      '#open' => true,
    ];

    $start_date = Drupal::request()->query->get('start');
    $form['content_log_filters']['date_range']['start_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Start date'),
      '#default_value' => ($start_date ?: ''),
    ];

    $end_date = Drupal::request()->query->get('end');
    $form['content_log_filters']['date_range']['end_date'] = [
      '#type' => 'date',
      '#title' => $this->t('End date'),
      '#default_value' => ($end_date ?: ''),
    ];

    $form['content_log_filters']['actions'] = [
      '#type' => 'actions',
    ];
    $form['content_log_filters']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
      '#name' => 'filter_results',
      '#button_type' => 'primary',
    ];

      $form['content_log_filters']['actions']['reset'] = [
        '#type' => 'submit',
        '#value' => $this->t('Reset'),
        '#name' => 'reset_filters',
        '#button_type' => 'secondary',
      ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {

    $start_date = $form_state->getValue('start_date');
    $end_date = $form_state->getValue('end_date');

    $params = [
      'query' => []
    ];
    if (!empty($start_date)) {
      $params['query']['start'] = $start_date;
    }
    if (!empty($end_date)) {
      $params['query']['end'] = $end_date;
    }

    $submit_button = $form_state->getTriggeringElement()['#name'];
    if ($submit_button == 'filter_results') {
      if (!empty($params['query'])) {
        Drupal::service('messenger')->addMessage('Filtering results by date.');
      }
      $url = Url::fromRoute('content_modification_log.report', [], $params);
    } elseif ($submit_button == 'export_results') {
      $url = Url::fromRoute('content_modification_log.export', [], $params);
    } else {
      Drupal::service('messenger')->addMessage('Filters cleared.');
      $url = Url::fromRoute('content_modification_log.report', [], []);
    }
    $form_state->setRedirectUrl($url);
  }
}
